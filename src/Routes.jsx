import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Login from "./pages/Login";
import MoviePage from "./pages/MoviePage";
import ProtectRoutes from "./ProtectRoutes";

export default function AppRoutes() {
  return (
    <BrowserRouter>
      <main>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="Login" element={<Login />} />

          <Route
            path="Movie"
            element={
              <ProtectRoutes>
                <MoviePage />
              </ProtectRoutes>
            }
          />
        </Routes>
      </main>
    </BrowserRouter>
  );
}
