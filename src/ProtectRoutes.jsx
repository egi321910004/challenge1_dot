import { Navigate } from "react-router-dom";

const ProtectRoutes = (props) => {
  const user = true;

  if (!user) return <Navigate to="/login" />;
  return props.children;
};

export default ProtectRoutes;
