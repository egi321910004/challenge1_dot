import { useState, useEffect } from "react";
import axios from "axios";
import MovieCard from "../component/MovieCard";
import Button from "../component/Button";
async function getMovies() {
  const res = await axios.get(
    `https://api.themoviedb.org/3/trending/all/day?api_key=803b94a9b50cd60a63fb6d53fbf6b1fe`
  );
  console.log(res.data.results);
  return res.data.results;
}
function MoviePage() {
  const [movies, setMovies] = useState("Loading");
  const [pageNo, setPageNo] = useState(1);
  useEffect(() => {
    getMovies(pageNo)
      .then((res) => {
        setMovies(res);
      })
      .catch((err) => {
        alert(err);
      });
  }, [pageNo]);
  if (movies === "Loading" || !movies || movies.length === 0)
    return (
      <div className="flex items-center justify-center h-screen bg-gradient-to-r from-rose-100 to-teal-100">
        <img
          src="/images/logobig.png"
          alt="loading"
          height="200px"
          width="200px"
        />
      </div>
    );
  else
    return (
      <div className="bg-gradient-to-r from-rose-100 to-teal-100 min-h-screen flex flex-col items-center h-full">
        <div className="flex flex-wrap justify-evenly">
          {movies.map((movie) => (
            <MovieCard movie={movie} />
          ))}
        </div>
        <div className="w-[250px] mt-5 pb-10 font-bold flex items-center justify-between">
          <Button
            onClick={() => {
              if (pageNo > 1) {
                setMovies("Loading");
                setPageNo(pageNo - 1);
              }
            }}
          >
            Previous
          </Button>
          {pageNo}
          <Button
            onClick={() => {
              if (pageNo < 20) {
                setMovies("Loading");
                setPageNo(pageNo + 1);
              }
            }}
          >
            Next
          </Button>
        </div>
      </div>
    );
}

export default MoviePage;
