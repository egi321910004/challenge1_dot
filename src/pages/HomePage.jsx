import React from "react";
import { Link } from "react-router-dom";
import Button from "../component/Button";
import Navbar from "../component/Navbar";

export default function HomePage() {
  return (
    <div>
      <Navbar />
      <div class="max-w bg-gradient-to-r from-rose-100 to-teal-100 shadow-sm rounded-lg mx-auto text-center py-80 mt-1">
        <h2 class="text-3xl leading-9 font-bold tracking-tight text-gray-800 sm:text-4xl sm:leading-10">
          Silahkan Login untuk dapat mengakses Movielist
        </h2>
        <div class="mt-8 flex justify-center">
          <div class="inline-flex rounded-md ">
            <Link to="/Login">
              <Button>Login</Button>
            </Link>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-center py-6 mt-4">
        <h1>Create by Egi Rizki ❤</h1>
      </div>
    </div>
  );
}
