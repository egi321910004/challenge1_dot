import React from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Button from "./Button";
export default function Navbar() {
  const navigate = useNavigate();
  const logout = () => {
    navigate("/login");
  };
  return (
    <nav className="w-full z-50 bg-white shadow-md">
      <div className="container mx-auto flex flex-wrap justify-between items-center px-4 py-3">
        <div className="flex md:gap-7">
          {/* <!-- LOGO --> */}
          <Link to={`/`}>
            <img src="/images/logoegi.ico" alt="Logo" />
          </Link>

          {/* <!-- SEARCH --> */}
          <div className="relative hidden lg:inline-block">
            <input
              name="search"
              className="px-5 py-1 bg-gray-200 lg:w-96 h-full rounded-lg focus:outline-none"
              placeholder="Cari disini .."
            />
            <span className="absolute inset-y-0 right-4 flex items-center">
              <img src="/icons/fi_search.svg" alt="search" />
            </span>
          </div>
        </div>

        <div className="flex gap-4 lg:gap-7 h-6 items-center justify-between">
          <Link className="" to="/Login">
            <Button>MovieList</Button>
          </Link>

          <button className="" onClick={logout}>
            <img src="/icons/iconlogout.svg" alt="movie" />
          </button>
        </div>
      </div>
    </nav>
  );
}
