import React from "react";

export default function Button(props) {
  return (
    <div>
      <button className="text-base mt-2 font-semibold text-white bg-gradient-to-r from-rose-300 to-teal-200 hover:from-teal-300 hover:to-rose-300 py-3 px-8 rounded-full hover:shadow-lg flex items-center justify-between">
        {props.children}
      </button>
    </div>
  );
}
